//
//  RemindersHomeViewController.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 02/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit
import CoreData

class RemindersHomeViewController: UIViewController {
    
    @IBOutlet weak var remindersTableView: UITableView!
    
    @IBOutlet weak var createNewReminderButton: UIBarButtonItem!
    
    var reminderType : ReminderType?
    
    var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    var reminderList = [Reminder]()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting dynamic sizing of TableViewCell Height
        self.remindersTableView.estimatedRowHeight = 180.0
        self.remindersTableView.rowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let reminderList = self.fetchObjects() {
            self.reminderList = reminderList
        }
        self.remindersTableView.reloadData()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - Button actions
    @IBAction func createNewReminderButtonTapped(_ sender: Any) {
        self.presentChooseReminderTypeAlert()
    }
    
    
    //MARK: - Helper Methods
    func presentChooseReminderTypeAlert() {
        let alert = UIAlertController(title: "Choose type of Reminder", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title: "One Time", style: UIAlertAction.Style.default, handler: { (action) in
            self.createNewReminder(with: ReminderType.OneTime)
        }))
        
        alert.addAction(UIAlertAction(title: "Recurring", style: UIAlertAction.Style.default, handler: { (action) in
            self.createNewReminder(with: ReminderType.Recurring)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true) {
            print("Alert Controller presented")
        }
    }
    
    ///Navigates to Create reminder screen with selected Reminder type
    func createNewReminder(with reminderType: ReminderType) {
        let createNewReminder = self.storyboard?.instantiateViewController(withIdentifier: Constants.CreateReminderVC.rawValue) as! CreateReminderViewController
        
        createNewReminder.reminderType = reminderType
        
        self.navigationController?.pushViewController(createNewReminder, animated: true)
        
    }
    
    ///Returns latest changes with respect to newly added or deleted reminders
    func fetchObjects() -> [Reminder]?{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "startDate", ascending: true)]
        
        if let result = try? self.context!.fetch(fetchRequest) as? [Reminder]{
            return result
        }else{
            return nil
        }
    }
}

//MARK: - TableView Datasource
extension RemindersHomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reminderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reminder = self.reminderList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReminderHomeCell.rawValue) as! ReminderHomeTableViewCell
        
        cell.configureCell(with: reminder, forIndexPath: indexPath)
        
        return cell
    }
}

//MARK: - TableView Delegate
extension RemindersHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let createNewReminder = self.storyboard?.instantiateViewController(withIdentifier: Constants.CreateReminderVC.rawValue) as! CreateReminderViewController
        self.reminderType = reminderList[indexPath.row].reminderType.map { ReminderType(rawValue: $0)! }
        createNewReminder.isReminderEditingOn = false
        createNewReminder.reminderType = reminderType
        createNewReminder.reminder = reminderList[indexPath.row]
        self.navigationController?.pushViewController(createNewReminder, animated: true)
    }
}
