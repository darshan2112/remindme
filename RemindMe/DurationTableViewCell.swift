//
//  DurationTableViewCell.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 04/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit

class DurationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var twentyFiveMinutes: UIButton!
    
    @IBOutlet weak var oneHour: UIButton!
    
    @IBOutlet weak var twoMonths: UIButton!
    
    var arrayOfButtons : [UIButton]!
    
    let selectionColor = UIColor.blue
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.arrayOfButtons = [twentyFiveMinutes, oneHour, twoMonths]
        
        //Initial button configuration
        for button in arrayOfButtons {
            button.layer.cornerRadius = 8.0
            button.layer.masksToBounds = true
            button.layer.borderWidth = 1.0
            button.layer.borderColor = self.selectionColor.cgColor
        }
    }
    
    //Visual changes for button
    @IBAction func twentyFiveButtonTapped(_ sender: Any) {
        self.resetButtonForNormalState()
        twentyFiveMinutes.setTitleColor(UIColor.white, for: UIControl.State.normal)
        twentyFiveMinutes.backgroundColor = selectionColor
    }
    
    @IBAction func oneHourButtonTapped(_ sender: Any) {
        self.resetButtonForNormalState()
        oneHour.setTitleColor(UIColor.white, for: UIControl.State.normal)
        oneHour.backgroundColor = selectionColor
    }
    
    @IBAction func twoMonthsButtonTapped(_ sender: Any) {
       self.resetButtonForNormalState()
        twoMonths.setTitleColor(UIColor.white, for: UIControl.State.normal)
        twoMonths.backgroundColor = selectionColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //Cell Configuration
    func configureCell(with reminderData: Reminder?, forIndexPath indexPath: IndexPath, isEditing: Bool) {
        if let reminderData = reminderData{
            for i in 0..<arrayOfButtons.count {
                
                arrayOfButtons[i].isEnabled = isEditing
                
                if i == Int(reminderData.duration){
                    arrayOfButtons[i].setTitleColor(UIColor.white, for: UIControl.State.normal)
                    arrayOfButtons[i].backgroundColor = selectionColor
                }else{
                    arrayOfButtons[i].setTitleColor(selectionColor, for: UIControl.State.normal)
                    arrayOfButtons[i].backgroundColor = UIColor.clear
                }
            }
        }
    }
    
    ///Method to reset visual changes for button state normal
    func resetButtonForNormalState(){
        for button in arrayOfButtons{
            button.setTitleColor(selectionColor, for: UIControl.State.normal)
            button.backgroundColor = UIColor.clear
        }
    }
}
