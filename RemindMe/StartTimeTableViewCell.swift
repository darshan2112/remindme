//
//  StartTimeTableViewCell.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 04/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit

class StartTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var timePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(with reminderData: Reminder?, forIndexPath indexPath: IndexPath, isEditing: Bool) {
        if let startDate = reminderData?.startDate, isEditing == false {
            self.timePicker.date = startDate
            self.timePicker.isEnabled = isEditing
        }else{
            self.timePicker.isEnabled = isEditing
        }
    }
}
