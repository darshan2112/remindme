//
//  ReminderObject.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 03/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import Foundation

struct ReminderObject {
    var reminderType: ReminderType
    var startDate: Date
    var startTime: Date
    var duration: Int?
    var frequency: Int?
    var title: String
    var id: String
}
