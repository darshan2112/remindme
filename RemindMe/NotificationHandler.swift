//
//  NotificationHandler.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 03/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import CoreData

class NotificationHandler {
    
    static let sharedInstance = NotificationHandler()
    
    private init() {}
    
    var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    lazy private var writeContext = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.newBackgroundContext()
    
    ///Method to combine "Remind at" time with "Start Date"
    func combineDateAndTime(date: Date, time: Date) -> Date {
        
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        var components = DateComponents()
        components.year = dateComponents.year
        components.month = dateComponents.month
        components.day = dateComponents.day
        components.hour = timeComponents.hour
        components.minute = timeComponents.minute
        components.second = timeComponents.second
        
        return calendar.date(from: components)!
    }
    
    
    ///Method to create local notification for notifications of reminder
    func createLocalNotification(forReminder reminder : Reminder){
        
        let notifications = Array(reminder.notifications!)
        
        for element in notifications{
            if let notificationInfo = element as? Notification, let fireDate = notificationInfo.fireAt {
                
                let notificationContent = UNMutableNotificationContent()
                notificationContent.title = reminder.title ?? ""
                notificationContent.body = reminder.reminderType ?? ""
                notificationContent.userInfo = ["id": notificationInfo.id!]
                notificationContent.sound = UNNotificationSound.default
                
                let notification = UNNotificationRequest(identifier: notificationInfo.id ?? "", content: notificationContent, trigger: notificationTrigger(forDate: fireDate))
                
                UNUserNotificationCenter.current().add(notification) { error in
                    print("Error scheduling user notification - \(error?.localizedDescription ?? "Empty error")")
                }
            }
        }
    }
    
    ///Method to create set of notifications based on the repeat type and duration selected
    func scheduleNotifications(forReminder reminder: ReminderObject) -> [NotificationObject]?{
        var notifications = [NotificationObject]()
        
        if reminder.reminderType == ReminderType.Recurring {
            
            let lastDate = Date(timeInterval: TimeInterval(self.getTimeIntervalForReminderObjectLastDate(forReminder: reminder)), since: self.combineDateAndTime(date: reminder.startDate as Date, time: reminder.startTime as Date))
            
            var fireDate = self.combineDateAndTime(date: reminder.startDate as Date, time: reminder.startTime as Date)
            
            //            Check if the fire date is less than the last date
            while (fireDate < lastDate){
                let uuid = UUID().uuidString
                let notificationObject = NotificationObject(reminder: reminder , fireAt: fireDate, id: uuid)
                
                //Append notifications object to create Notfication later on
                notifications.append(notificationObject)
                
                // Append interval based on the repeat mode Chosen by user
                fireDate = fireDate.addingTimeInterval(TimeInterval(getTimeIntervalForReapeatType(reminderRepeatMode: RepeatMode(rawValue: Int(reminder.frequency!))!)))
            }
            
        }else{
            
            let fireDate = self.combineDateAndTime(date: reminder.startDate as Date, time: reminder.startTime as Date)
            
            let uuid = UUID().uuidString
            let notificationObject = NotificationObject(reminder: reminder , fireAt: fireDate, id: uuid)
            notifications.append(notificationObject)
        }
        
        return notifications
    }
    
    ///Method returns time interval to be added to RemindAt date to get the Number of reminders to be scheduled
    func getTimeIntervalForReminderObjectLastDate(forReminder reminder: ReminderObject) -> Int {
        switch reminder.duration!{
        case Duration.TwentyFivemins.rawValue:
            return TWENTY_FIVE_MINS
            
        case Duration.Onehour.rawValue:
            return ONE_HOUR
            
        case Duration.TwoMonths.rawValue:
            return TWO_MONTHS
            
        default:
            return 0
        }
    }
    
    ///Method to get time interval to be appended to "fire At" date to get the "next Fire at" date
    func getTimeIntervalForReapeatType(reminderRepeatMode: RepeatMode) -> Int{
        switch reminderRepeatMode {
        case RepeatMode.Minute:
            return 60
        case RepeatMode.Hourly:
            return 60 * 60
            
        case RepeatMode.Daily:
            return 60 * 60 * 24
            
        case RepeatMode.Weekly:
            return 60 * 60 * 24 * 7
            
        case RepeatMode.Monthly:
            return 60 * 60 * 24 * 7
            
        case RepeatMode.Yearly:
            return 60 * 60 * 24 * 365
        }
        
    }
    
    func intervalNotificationTrigger(for interval: TimeInterval, shouldRepeat repeats: Bool = false) -> UNNotificationTrigger {
        return UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: repeats)
    }
    
    func notificationTrigger(forDate date: Date) -> UNNotificationTrigger {
        let currentTime = Date().timeIntervalSince1970
        let targetTime = date.timeIntervalSince1970
        
        guard targetTime > currentTime else { fatalError("Cannot set reminder in past") }
        return intervalNotificationTrigger(for: targetTime - currentTime)
    }
    
    ///Function to remove particular notification on editing or deleting reminder
    func removeLocalNotification(withId id: String) {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in
            guard notifications.isEmpty == false else { return }
            let identifiers = (notifications.filter({ $0.identifier == id }).map{ $0.identifier })
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
        }
    }
    
    
    ///Method to delete all local notifications and notifications in reminder object
    func deleteAllNotifications(forReminder reminder: Reminder){
        guard let notifications = Array(arrayLiteral: reminder.notifications) as? [Notification] else { return }
        notifications.forEach {
            //We first remove local notifications
            NotificationHandler.sharedInstance.removeLocalNotification(withId: $0.id!)
            //Remove corresponding notification objectt
            writeContext?.delete($0)
            save()
        }
    }
    
    private func save() {
        do{
            try writeContext?.save()
        }catch let error{
            print("Error deleting notifications. Error: \(error.localizedDescription)")
        }
    }
}
