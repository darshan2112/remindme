//
//  CreateReminderViewController.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 02/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

protocol TitleCellDelegate {
    func didSetTitle(withText title: String)
}


import UIKit
import CoreData

class CreateReminderViewController: UIViewController, TitleCellDelegate{
    
    var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var newReminderTableView: UITableView!
    
    var reminder : Reminder?
    
    var isReminderEditingOn = true
    
    //MARK: - Reminder Object Elements
    ///Reminder type is used to configure the tableView cells. We hide frequency and duration cell when the reminder type is set to OneTime
    
    var reminderID : String!
    
    var reminderType : ReminderType!
    
    var repeatMode: RepeatMode?
    
    var duration: Duration?
    
    var startTime: NSDate!
    
    var startDate: NSDate!
    
    var reminderTitle: String?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newReminderTableView.estimatedRowHeight = 50.0
        self.newReminderTableView.rowHeight = UITableView.automaticDimension
        
        self.configureRightnavigationBarButton()
        
        //If the reminder is a existing reminder, we load details to class properties which are used to check if all parameters required to create reminder are entered
        
        if let reminder = self.reminder{
            self.startDate = reminder.startDate as? NSDate
            self.startTime = reminder.startTime as? NSDate
            self.reminderTitle = reminder.title
            if reminderType == ReminderType.Recurring{
                self.duration = Duration(rawValue: Int(reminder.duration))
                self.repeatMode = RepeatMode(rawValue: Int(reminder.frequency))
            }
        }
        
        self.configureInitialViewForRightBarButton()
        
        self.setReminderID()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureReminderTitleForReminder(reminder: self.reminder)
    }
    
    //MARK: - Button Actions
    @IBAction func saveButtonTapped(_ sender: Any) {

        if self.saveButton.title == "Save" {

            let newReminderObject = ReminderObject(reminderType: self.reminderType, startDate: startDate as Date, startTime: self.startTime as Date, duration: self.duration?.rawValue, frequency: repeatMode?.rawValue, title: reminderTitle!, id: self.reminderID)

            let notificationObjects = NotificationHandler.sharedInstance.scheduleNotifications(forReminder: newReminderObject)
  
            if let newNotificationObjects = notificationObjects{
                self.updateDatabase(newNotifications: newNotificationObjects)
            }else{
                print("Error scheduling notifications")
            }
            
            self.saveButton.title = "Edit"
            self.deleteButton.isHidden = false
            
        } else{
            self.isReminderEditingOn = true
            self.saveButton.title = "Save"
            self.deleteButton.isHidden = true
            self.newReminderTableView.reloadData()
        }
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        if let reminder = self.reminder{
            
            //Delete Local notifications & notification objects corresponding to reminder
            NotificationHandler.sharedInstance.deleteAllNotifications(forReminder: reminder)
            
            //Remove reminder
            context?.delete(reminder)
            
            do{
                try context?.save()
                
            }catch let error{
                print("Error deleting reminder. Error Description: \(error.localizedDescription)")
            }
            self.navigationController?.popViewController(animated: true)
        }else{
            //Present alert controller
            let alertViewController = UIAlertController(title: "Error", message: "Error deleting reminder", preferredStyle: UIAlertController.Style.alert)
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                print("Alert action dismissed")
            }))
            
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func timePickerValueChanged(_ sender: Any) {
        let  picker = sender as! UIDatePicker
        self.startTime = picker.date as NSDate
        
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func repeatModeSetToMinute(_ sender: Any) {
        self.repeatMode = RepeatMode.Minute
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func repeatModeSetToHour(_ sender: Any) {
        self.repeatMode = RepeatMode.Hourly
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func repeatModeSetToDaily(_ sender: Any) {
        self.repeatMode = RepeatMode.Daily
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func repeatModeSetToWeekly(_ sender: Any) {
        self.repeatMode = RepeatMode.Weekly
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func repeatModeSetToMonthly(_ sender: Any) {
        self.repeatMode = RepeatMode.Monthly
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func repeatModeSetToYearly(_ sender: Any) {
        self.repeatMode = RepeatMode.Yearly
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func duration25MinsSelected(_ sender: Any) {
        self.duration = Duration.TwentyFivemins
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
        
    }
    
    @IBAction func duration1hourTapped(_ sender: Any) {
        self.duration = Duration.Onehour
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
        
    }
    
    @IBAction func duration2MonthsTapped(_ sender: Any) {
        self.duration = Duration.TwoMonths
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    @IBAction func startDateChanged(_ sender: Any) {
        let  picker = sender as! UIDatePicker
        self.startDate = picker.date as NSDate
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
    
    //MARK: - Helper methods
    
    func updateDatabase(newNotifications: [NotificationObject]){
        
        for element in newNotifications {
            _ = Notification.notificationWithNotificationInfo(notificationInfo: element, inManagedObjectContext: context!)
        }
        do{
            try context!.save()
            print("Successfully added reminder")
            
            //We create local notifications after successfully saving notifications & reminder to DB
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
            request.predicate = NSPredicate(format: "id = %@", self.reminderID)
            
            if let reminder = (try? context!.fetch(request))?.first as? Reminder{
                NotificationHandler.sharedInstance.createLocalNotification(forReminder: reminder)
            }
            
        }catch let error{
            print("Error saving new reminder. Error Description: \(error)")
        }
        
    }
    
    //We fetch the reminderID from reminder object if its is existing reminder. Else, we get a new id
    func setReminderID(){
        if self.reminder != nil{
            self.reminderID = reminder?.id!
        }else{
            self.reminderID = UUID().uuidString
        }
    }
    
    ///Configure Reminder screen title
    func configureReminderTitleForReminder(reminder: Reminder?){
        if let reminder = reminder{
            self.navigationItem.title =  reminder.title
        }else{
            self.navigationItem.title = "New " + self.reminderType.rawValue + " Reminder"
        }
    }
    
    //Hide and Unhide Delete button
    func configureInitialViewForRightBarButton(){
        if self.saveButton.title == "Save"{
            self.deleteButton.isHidden = true
        }else{
            self.deleteButton.isHidden = false
        }
    }
    
    //Cell configuration based on the reminder type
    func configureCellFor(tableView: UITableView, reminderType: ReminderType, index: IndexPath) -> UITableViewCell {
        
        
        if reminderType == ReminderType.OneTime {
            switch index.row {
                
            case 1:
               let cell = tableView.dequeueReusableCell(withIdentifier: Constants.StartTimeCell.rawValue, for: index) as! StartTimeTableViewCell
                
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                
                return cell

                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.StartDateCell.rawValue , for: index) as! StartDateTableViewCell
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReminderTitleCell.rawValue, for: index) as! ReminderTitleTableViewCell
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                
                cell.delegate = self
                return cell
            }
            
        } else {
            switch index.row {
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.StartTimeCell.rawValue, for: index) as! StartTimeTableViewCell
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.RepeatCell.rawValue, for: index) as! RepeatTableViewCell
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.DurationCell.rawValue, for: index) as! DurationTableViewCell
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                return cell
                
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.StartDateCell.rawValue , for: index) as! StartDateTableViewCell
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReminderTitleCell.rawValue, for: index) as! ReminderTitleTableViewCell
                
                cell.configureCell(with: self.reminder, forIndexPath: index, isEditing: self.isReminderEditingOn)
                
                cell.delegate = self
                return cell
            }
            
        }
        
    }
    
    func shouldEnableSaveButton() -> Bool {
        if self.saveButton.title == "Save" {
            if self.reminderType == ReminderType.OneTime{
                if self.reminderTitle != nil && self.startDate != nil && self.startTime != nil {
                    return true
                }else{
                    return false
                }
            }else {
                if self.reminderTitle != nil && self.startDate != nil && self.startTime != nil && self.repeatMode != nil && self.duration != nil {
                    return true
                }else{
                    return false
                }
            }
        }else {
            return true
        }
    }
    
    func configureRightnavigationBarButton(){
        if self.reminder == nil {
            self.saveButton.title = "Save"
        }else{
            self.saveButton.title = "Edit"
        }
        
        self.navigationItem.rightBarButtonItem = self.saveButton
    }
    
    //MARK: TitleCellDelegate
    func didSetTitle(withText title: String) {
        self.reminderTitle = title
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
    }
}

    //MARK: - TableView Configuration
extension CreateReminderViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.reminderType == ReminderType.OneTime{
            return 3
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.saveButton.isEnabled = self.shouldEnableSaveButton()
        return configureCellFor(tableView: tableView, reminderType: self.reminderType, index: indexPath)
    }
    
}
