//
//  RepeatTableViewCell.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 04/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit

class RepeatTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var minuteButton: UIButton!
    
    @IBOutlet weak var hourButton: UIButton!
    
    @IBOutlet weak var dailyButton: UIButton!
    
    @IBOutlet weak var weeklyButton: UIButton!
    
    @IBOutlet weak var monthlyButton: UIButton!
    
    @IBOutlet weak var yearlyButton: UIButton!
    
    var arrayOfButtons : [UIButton]!
    
    let selectionColor = UIColor.blue
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.arrayOfButtons = [minuteButton, hourButton, dailyButton, weeklyButton, monthlyButton, yearlyButton]
        
        //Button configuration
        for button in arrayOfButtons{
            button.layer.cornerRadius = 8.0
            button.layer.masksToBounds = true
            button.layer.borderWidth = 1.0
            button.layer.borderColor = self.selectionColor.cgColor
        }
    }
    
    //MARK: - Button visual changes when any of the buttons is selected
    @IBAction func minButtontapped(_ sender: Any) {
        resetButtonSelection()
        self.minuteButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.minuteButton.backgroundColor = selectionColor
    }
    @IBAction func hourButtontapped(_ sender: Any) {
        resetButtonSelection()
        self.hourButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.hourButton.backgroundColor = selectionColor
    }
    
    @IBAction func dailyButtonTapped(_ sender: Any) {
        resetButtonSelection()
        self.dailyButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.dailyButton.backgroundColor = selectionColor
    }
    
    @IBAction func weeklyButtonTapped(_ sender: Any) {
        resetButtonSelection()
        self.weeklyButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.weeklyButton.backgroundColor = selectionColor
    }
    
    @IBAction func monthlyButtonTapped(_ sender: Any) {
        resetButtonSelection()
        self.monthlyButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.monthlyButton.backgroundColor = selectionColor
    }
    
    @IBAction func yearlyButtonTapped(_ sender: Any) {
        resetButtonSelection()
        self.yearlyButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.yearlyButton.backgroundColor = selectionColor
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    ///Reset visual changes for button state normal
    func resetButtonSelection(){
        for button in arrayOfButtons{
            button.setTitleColor(selectionColor, for: UIControl.State.normal)
            button.backgroundColor = UIColor.clear
        }
    }
    
    //Cell Configuration
    func configureCell(with reminderData: Reminder?, forIndexPath indexPath: IndexPath, isEditing: Bool) {
        if let reminderData = reminderData{
            for i in 0..<arrayOfButtons.count {
                
                arrayOfButtons[i].isEnabled = isEditing
                
                if i == Int(reminderData.frequency){
                    arrayOfButtons[i].setTitleColor(UIColor.white, for: UIControl.State.normal)
                    arrayOfButtons[i].backgroundColor = selectionColor
                }else{
                    arrayOfButtons[i].setTitleColor(selectionColor, for: UIControl.State.normal)
                    arrayOfButtons[i].backgroundColor = UIColor.clear
                }
            }
        }
    } 
}
