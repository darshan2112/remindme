//
//  NotificationObject.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 03/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import Foundation

struct NotificationObject {
    var reminder: ReminderObject
    var fireAt: Date
    var id: String
}
