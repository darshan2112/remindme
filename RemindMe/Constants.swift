//
//  Constants.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 02/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import Foundation

enum Constants : String {
    case ReminderHomeCell
    case CreateReminderVC
    case RepeatCell
    case StartTimeCell
    case StartDateCell
    case DurationCell
    case ReminderTitleCell
    case ReminderDetailSegue
}

enum ReminderType : String {
    case OneTime = "OneTime"
    case Recurring = "Recurring"
}

enum RepeatMode: Int {
    case Minute = 0
    case Hourly
    case Daily
    case Weekly
    case Monthly
    case Yearly
}

enum Duration: Int {
    case TwentyFivemins = 0
    case Onehour
    case TwoMonths
    
}

var TWENTY_FIVE_MINS = 60 * 25
var ONE_HOUR = 60*60
var TWO_MONTHS = 60*60*24*60
