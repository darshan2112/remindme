//
//  Extensions.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 04/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension Reminder{
    
    class func reminderWithNotificationInfo(reminderInfo: ReminderObject, inManagedObjectContext context: NSManagedObjectContext) -> Reminder?{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
        request.predicate = NSPredicate(format: "id = %@", reminderInfo.id)
        
        // If reminder object with the id exist, we edit the same reminder. Else, we create a new reminder
        
        if let reminder = (try? context.fetch(request))?.first as? Reminder{
            
            reminder.id = reminderInfo.id
            reminder.title = reminderInfo.title
            if reminderInfo.reminderType == ReminderType.Recurring {
                reminder.duration = Int16(reminderInfo.duration!)
                reminder.frequency = Int16(reminderInfo.frequency!)
            }
            reminder.reminderType = reminderInfo.reminderType.rawValue
            reminder.startDate = reminderInfo.startDate as Date
            reminder.startTime = reminderInfo.startTime as Date
            
            return reminder
            
        }else if let reminder = NSEntityDescription.insertNewObject(forEntityName: "Reminder", into: context) as? Reminder{
            
            reminder.id = reminderInfo.id
            reminder.title = reminderInfo.title
            if reminderInfo.reminderType == ReminderType.Recurring {
                reminder.duration = Int16(reminderInfo.duration!)
                reminder.frequency = Int16(reminderInfo.frequency!)
            }
            reminder.reminderType = reminderInfo.reminderType.rawValue
            reminder.startDate = reminderInfo.startDate as Date
            reminder.startTime = reminderInfo.startTime as Date
            return reminder
        }
        
        return nil
    }
}

extension Notification {
    class func notificationWithNotificationInfo(notificationInfo: NotificationObject, inManagedObjectContext context: NSManagedObjectContext) -> Notification?{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notification")
        request.predicate = NSPredicate(format: "id = %@", notificationInfo.id)
        
        //If notification exist, we delete that notification and corresponding notification and then create notification object
        if let notification = (try? context.fetch(request))?.first as? Notification{
            NotificationHandler.sharedInstance.removeLocalNotification(withId: notification.id!)
            context.delete(notification)
            try? context.save()
        }
        
        if let notification = NSEntityDescription.insertNewObject(forEntityName: "Notification", into: context) as? Notification{
            notification.id = notificationInfo.id
            notification.fireAt = notificationInfo.fireAt as Date
            notification.remindObject = Reminder.reminderWithNotificationInfo(reminderInfo: notificationInfo.reminder, inManagedObjectContext: context)
            
            return notification
        }
        
       return nil
    }
    
}
