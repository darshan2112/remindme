//
//  StartDateTableViewCell.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 04/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit

class StartDateTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.datePicker.minimumDate = Date()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //Configure cell
    func configureCell(with reminderData: Reminder?, forIndexPath indexPath: IndexPath, isEditing: Bool) {
        
        if let startDate = reminderData?.startDate {
            self.datePicker.date = startDate
            self.datePicker.isEnabled = isEditing
        }
    }
    
}
