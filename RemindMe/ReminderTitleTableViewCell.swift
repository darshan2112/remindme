//
//  ReminderTitleTableViewCell.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 04/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit

class ReminderTitleTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    
    @IBOutlet weak var reminderTextfield: UITextField!
    
    var delegate : TitleCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reminderTextfield.delegate = self
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(with reminderData: Reminder?, forIndexPath indexPath: IndexPath, isEditing: Bool){
        if reminderData != nil {
            self.reminderTextfield.text = reminderData?.title
            self.reminderTextfield.isEnabled = isEditing
        }
    }
    
    //MARK: - Textfeild Delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("Reminder textfield began editing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if textField.hasText {
            delegate?.didSetTitle(withText: textField.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
