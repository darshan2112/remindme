//
//  ReminderHomeTableViewCell.swift
//  RemindMe
//
//  Created by Darshan Shivakumar on 02/02/17.
//  Copyright © 2017 Darshan Shivakumar. All rights reserved.
//

import UIKit


class ReminderHomeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var reminderTitleLabel: UILabel!
    
    @IBOutlet weak var repeatModeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(with reminderData: Reminder, forIndexPath indexPath: IndexPath) {
        self.reminderTitleLabel.text = reminderData.title
        self.repeatModeLabel.text = reminderData.reminderType
        
    }
}
